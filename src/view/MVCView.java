package view;

import model.logic.MVCModelo;
import model.logic.UberTrip;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Crear Arreglo Dinamico de UberTrip");
			System.out.println("2. Consultar Viajes por hora");
			System.out.println("3. Organizar por shell sort");
			System.out.println("4. Organizar por Merge sort");
			System.out.println("5. Organizar por Quick sort");
			System.out.println("6. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			System.out.println("El total de viajes leidos fue de: " + modelo.darTamano() + "\n");
			
			System.out.println("Primer viaje---> \n	Zona origen: " + modelo.darLista().darElemento(0).darIdOrigen());
			System.out.println("	Zona destino: " + modelo.darLista().darElemento(0).darIdDestino());
			System.out.println("	Hora: " + modelo.darLista().darElemento(0).darHod());
			System.out.println("	Tiempo promedio: " + modelo.darLista().darElemento(0).darTiempoPromedio() + "\n");
			
			System.out.println("Ultimo viaje---> \n	Zona origen: " + modelo.darLista().darElemento(modelo.darTamano()-1).darIdOrigen());
			System.out.println("	Zona destino: " + modelo.darLista().darElemento(modelo.darTamano()-1).darIdDestino());
			System.out.println("	Hora: " + modelo.darLista().darElemento(modelo.darTamano()-1).darHod());
			System.out.println("	Tiempo promedio: " + modelo.darLista().darElemento(modelo.darTamano()-1).darTiempoPromedio() + "\n");
		}
		
		public void printViajesConsulta(Comparable<UberTrip>[] arregloConsulta)
		{
			for(int i = 0; i < 10; i++)
			{
				UberTrip actual = (UberTrip) arregloConsulta[i];
				int sourceid = actual.darIdOrigen();
				int dstid = actual.darIdDestino();
				int hod = actual.darHod();
				double meanTT = actual.darTiempoPromedio();
				double sdtt = actual.darDesviacionEstandar(); 
				double geometric = actual.darTPromedioGeometrico();
				double gsdtt = actual.darDesvEstandarGeometrica();
				System.out.println("Viaje " + (i+1) + ": " + sourceid + ", " + dstid + ", " + hod + ", " + meanTT + ", " + sdtt + ", " + geometric + ", " + gsdtt);
			}
			
			int tamanio = arregloConsulta.length;
			
			for(int j = (tamanio-10); j < tamanio; j++)
			{
				UberTrip actual = (UberTrip) arregloConsulta[j];
				int sourceid = actual.darIdOrigen();
				int dstid = actual.darIdDestino();
				int hod = actual.darHod();
				double meanTT = actual.darTiempoPromedio();
				double sdtt = actual.darDesviacionEstandar(); 
				double geometric = actual.darTPromedioGeometrico();
				double gsdtt = actual.darDesvEstandarGeometrica();
				System.out.println("Viaje " + (j + 1) + ": " + sourceid + ", " + dstid + ", " + hod + ", " + meanTT + ", " + sdtt + ", " + geometric + ", " + gsdtt);
			}
			
		}
}
