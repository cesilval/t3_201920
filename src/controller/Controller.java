package controller;

import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.UberTrip;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	private Comparable<UberTrip>[] arregloConsulta;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo(11000000);
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
				    modelo.cargar();
				    view.printModelo(modelo);						
					break;

				case 2:
					System.out.println(" \nDar hora a consultar: ");
					dato = lector.next();
					arregloConsulta = modelo.consultarViajesPorHora(Integer.parseInt(dato));
					System.out.println("El numero de viajes resultantes de la consulta fue: " + arregloConsulta.length +"\n");					
					break;

				case 3:
					//shellsort
					long startTime = System.currentTimeMillis();
					modelo.shellSort(arregloConsulta);
					long endTime = System.currentTimeMillis();
					long temp = endTime -startTime;
					System.out.println("El tiempo que se demoro el algoritmo fue de: " + temp + " milisegundos");
					view.printViajesConsulta(arregloConsulta);
					break;

				case 4:
					//mergesort		
					long startTimeMerge = System.currentTimeMillis();
					modelo.mergeSort(arregloConsulta);
					long endTimeMerge = System.currentTimeMillis();
					long temp2 = endTimeMerge -startTimeMerge;
					System.out.println("El tiempo que se demoro el algoritmo fue de: " + temp2 + " milisegundos");
					view.printViajesConsulta(arregloConsulta);
					break;

				case 5: 
					//quicksort	
					long startTimeQuick = System.currentTimeMillis();
					modelo.QuickSort(arregloConsulta);
					long endTimeQuick = System.currentTimeMillis();
					long temp3 = endTimeQuick -startTimeQuick;
					System.out.println("El tiempo que se demoro el algoritmo fue de: " + temp3 + " milisegundos");
					view.printViajesConsulta(arregloConsulta);
					break;	
					
				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
