package model.data_structures;

public class ShellSort 
{
	
	public ShellSort()
	{
		
	}

	public static void shellSort( Comparable[] a )
	{
		int N = a.length;
		int h = 1;
		while (h < N/3) h = 3*h + 1; 
		while (h >= 1)
		{ 
			for (int i = h; i < N; i++)
			{
				for (int j = i; j >= h && less( a[ j ], a[ j - h ] ); j -= h)
					exch(a, j, j-h);
			}
			h = h/3;
		}
	}

	private static void exch( Comparable[] a, int j, int i ) 
	{
		Comparable x = a[ j ];
		a[j] = a[i];
		a[i] = x;

	}

	private static boolean less( Comparable comparable, Comparable comparable2 )  
	{
		return comparable.compareTo( comparable2 ) < 0? true: false;
	}

}

