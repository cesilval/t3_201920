package model.data_structures;

public class MergeSort 
{
	private static Comparable[] auxiliar;

	public static void merge( Comparable[] a, int lo, int mid, int hi)
	{ 
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) 
			auxiliar[k] = a[k];
		for (int k = lo; k <= hi; k++) 
			if (i > mid) a[k] = auxiliar[j++];
			else if (j > hi ) a[k] = auxiliar[i++];
			else if (less(auxiliar[j], auxiliar[i])) a[k] = auxiliar[j++];
			else a[k] = auxiliar[i++];
	}
	
	public static void mergeSort(Comparable[] a)
	{
		auxiliar = new Comparable[a.length];
		sort(a, 0, a.length - 1);
	}
	private static void sort(Comparable[] a, int lo, int hi)
	{ 
		if (hi <= lo) return;
		int mid = lo + (hi - lo)/2;
		sort(a, lo, mid); 
		sort(a, mid+1, hi); 
		merge(a, lo, mid, hi); 
	}

	private static boolean less( Comparable comparable, Comparable comparable2 )  
	{
		return comparable.compareTo( comparable2 ) < 0? true: false;
	}


}
