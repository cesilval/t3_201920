package model.data_structures;


public class ArregloDinamico<T extends Comparable<T>> implements IArregloDinamico<T> {
	/**
	 * Capacidad maxima del arreglo
	 */
	private int tamanoMax;
	/**
	 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
	 */
	private int tamanoAct;
	/**
	 * Arreglo de elementos de tamaNo maximo
	 */
	private T elementos[ ];

	/**
	 * Construir un arreglo con la capacidad maxima inicial.
	 * @param max Capacidad maxima inicial
	 */
	public ArregloDinamico( int max )
	{
		elementos = (T[]) new Comparable[max];
		tamanoMax = max;
		tamanoAct = 0;
	}

	public void agregar( T dato )
	{
		if ( tamanoAct == tamanoMax )
		{  // caso de arreglo lleno (aumentar tamaNo)
			tamanoMax = 2 * tamanoMax;
			T [ ] copia = elementos;
			elementos = (T[]) new Comparable[tamanoMax];
			for ( int i = 0; i < tamanoAct; i++)
			{
				elementos[i] = copia[i];
			} 
		}	
		elementos[tamanoAct] = dato;
		tamanoAct++;
	}

	public int darCapacidad() {
		return tamanoMax;
	}

	public int darTamano() {
		return tamanoAct;
	}

	public T darElemento(int i) 
	{

		return elementos[i];

	}
	public void cambiarElementos(  int j, int i )
	{
		T x = elementos[ j ];
		elementos[j] = elementos[i];
        elementos[i] = x;
	} 

	public T buscar(T dato) {

		for(int i = 0; i < tamanoAct; i++)
		{				
			if(elementos[i].compareTo(dato) == 0)
			{
				return elementos[i];
			}	
		}

		return null;
	}

	public T eliminar(T dato) {

		for(int i = 0; i < tamanoAct; i++)
		{
			if(elementos[i].compareTo(dato) == 0)
			{
				for(int j = i+1; j < tamanoAct; j++)
				{
					elementos[j-1] = elementos[j]; 
				}

				return elementos[i];
			}
		}

		return null;
	}

	public T[] darElementos() {
		return elementos;
	}



	}



