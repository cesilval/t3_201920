package model.data_structures;


public class QuickSort
{
	
	private static boolean less(Comparable v, Comparable w )
	{
		return v.compareTo(w) < 0;
	}
	
	private static void exch(Comparable[] a, int i, int j)
	{
		Comparable swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}
	
	private static int partition(Comparable[] a, int lo, int hi)
	{
		int i = lo;
		int j = hi+1;
		while(true)
		{
			while (less(a[++i],a[lo]))
			{
				if(i == hi)
				{
					break;
				}
			}
			
			while(less(a[lo], a[--j]))
			{
				if(j==0)
				{
					break;
				}
			}
			
			if(i >= j)
			{
				break;
			}
			
			exch(a,i,j);
		}
		
		exch(a,lo,j);
		return j;
	}

	public static void sort(Comparable[] a)
	{
		shuffle(a);
		sort(a, 0, a.length - 1);
		
	}
	
	private static void sort(Comparable[] a, int lo, int hi)
	{
		if(hi<=lo)return;
		int j = partition(a,lo,hi);
		sort(a,lo,j-1);
		sort(a,j+1,hi);
	}
	
	private static void shuffle(Comparable[] a)
	{
		int tamanio = a.length;
		Comparable[] x = new Comparable[tamanio];
		
		for(int i = 0; i < tamanio; i++)
		{
			int posAleatoria = (int) (Math.random() * tamanio);
			x[i]=a[posAleatoria];
			a[posAleatoria] = a[i];
			a[i]=x[i];	
		}
	}
	
	
	
}
