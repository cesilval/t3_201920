package model.logic;

public class UberTrip implements Comparable<UberTrip> 
{
	/**
	 *--------ATRIBUTOS-------------
	 */
	
	private int idOrigen;
	
	private int idDestino;
	
	private int hod;
	
	private double tiempoPromedio;
	
	private double desviacionEstandar;
	
	private double tPromedioGeometrico;
	
	private double desvEstandarGeometrica;
	
	public UberTrip(int pIdOrigen, int pIdDestino, int pHod, double pTiempoPromedio, double pDesviacionEstandar, double pTpromedioG, double pDesvEstGeo)
	{
		idOrigen = pIdOrigen;
		idDestino = pIdDestino;
		hod = pHod;
		tiempoPromedio = pTiempoPromedio;
		desviacionEstandar = pDesviacionEstandar;
		tPromedioGeometrico = pTpromedioG;
		desvEstandarGeometrica = pDesvEstGeo;
	}
	
	public int darIdOrigen( )
	{
		return idOrigen;
	}
	
	public int darIdDestino( )
	{
		return idDestino;
	}
	
	public int darHod( )
	{
		return hod;
	}
	
	public double darTiempoPromedio( )
	{
		return tiempoPromedio;
	}
	
	public double darDesviacionEstandar( )
	{
		return desviacionEstandar;
	}
	
	public double darTPromedioGeometrico( )
	{
		return tPromedioGeometrico;
	}
	
	public double darDesvEstandarGeometrica( )
	{
		return desvEstandarGeometrica;
	}	

	@Override
	public int compareTo(UberTrip arg0) 
	{
		//Toca hacer un proceso m�s largo para que ordene bien los numeros por decimal.
		
		double x = tiempoPromedio - arg0.darTiempoPromedio();
		int r = 0;
		if(x > 0.00)
		{
			r = 1;
		}
		else if(x < 0.00)
		{
			r = -1;
		}
		else if(x == 0.00)
		{
			r = (int)( desviacionEstandar - arg0.darDesviacionEstandar( ));
		}
		
		return r;
	}
	
}
