package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.MergeSort;
import model.data_structures.ShellSort;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private IArregloDinamico<UberTrip> datos;
	
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo(int pCapacidad)
	{
		datos = new ArregloDinamico<UberTrip>(pCapacidad);
	}
	
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	
	public int darTamano()
	{
		return datos.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(UberTrip dato)
	{	
		datos.agregar(dato);
	}
	
	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public void eliminar(UberTrip dato)
	{
		datos.eliminar(dato);
	}
	
	public IArregloDinamico<UberTrip> darLista()
	{
		return datos;
	}
	
	public void cargar( )
	{

		CSVReader reader = null;
		try
		{
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"));
			for(String[] nextLine : reader)
			{
				try
				{
					UberTrip viaje = new UberTrip(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
					datos.agregar(viaje);
				}
				catch ( Exception e )
				{

				}

			}

		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null) 
			{
				try 
				{
					reader.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}

		}
	}


	
	public Comparable<UberTrip>[] consultarViajesPorHora(int pHora)
	{
		ArregloDinamico<UberTrip> aD = new ArregloDinamico<UberTrip>(2048);
		int totalObjArreglo = 0;
		
		for(int i = 0; i < datos.darTamano(); i++)
		{
			if(datos.darElemento(i).darHod() == pHora)
			{
				aD.agregar(datos.darElemento(i));
				totalObjArreglo++;
			}
		}
		
		Comparable<UberTrip>[] x = (Comparable<UberTrip>[]) new Comparable[totalObjArreglo];
		
		for(int j = 0; j < totalObjArreglo; j++)
		{
			x[j]=aD.darElemento(j);
		}
		
		return x;
		
	}
	
	public Comparable<UberTrip>[] shellSort( Comparable<UberTrip>[] c )
	{
		model.data_structures.ShellSort.shellSort(c);
		return c;
	}
	
	public Comparable<UberTrip>[] mergeSort( Comparable<UberTrip>[] c )
	{
		MergeSort.mergeSort(c);
		return c;
	}
	
	public Comparable<UberTrip>[] QuickSort( Comparable<UberTrip>[] c )
	{
		model.data_structures.QuickSort.sort(c);
		return c;
	}

}
