package test.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.ShellSort;



public class TestShellSort 

{

	private Comparable<String>[] lista;

	@Before
	public void setUp1( ) 
	{
		lista = new Comparable[10];
		
		lista[0] = "1235";
		lista[1] = "2545";
		lista[2] = "3547";
		lista[3] = "46343";
		lista[4] = "5345635";
		lista[5] = "65768888";
		lista[6] = "733333333";
		lista[7] = "8555555555";
		lista[8] = "94333343434";
		lista[9] = "102132443434";

	}

	public void setUp2( ) 
	{
		
		lista[0] = "103";
		lista[1] = "94";
		lista[2] = "82";
		lista[3] = "71";
		lista[4] = "67";
		lista[5] = "54";
		lista[6] = "40";
		lista[7] = "39";
		lista[8] = "26";
		lista[9] = "12";
	}
	
	public void setUp3( ) 
	{
		
		lista[0] = "1045";
		lista[1] = "43445";
		lista[2] = "87685";
		lista[3] = "7787497";
		lista[4] = "69334529";
		lista[5] = "54674827";
		lista[6] = "45423457";
		lista[7] = "379490";
		lista[8] = "267845";
		lista[9] = "145673";
	}

	@Test
	public void testListaOrdenadaAscendentemente( ) 
	{
		setUp1( );
		ShellSort.shellSort(lista);
		
		boolean a = true;
		int cont = 0;
		while ( cont < lista.length - 1 && a )
		{
			a = lista[cont].compareTo( (String) lista[cont + 1] ) < 0? true: false;
			cont++;
		}
		
		assertTrue( lista != null );
		assertTrue( a );
	}
	
	public void testListaOrdenadaDescendentemente( ) 
	{
		setUp2( );
		ShellSort.shellSort(lista);
		
		boolean a = true;
		int cont = 0;
		while ( cont < lista.length && a )
		{
			a = lista[cont].compareTo( (String) lista[cont + 1] ) < 0? true: false;
			cont++;
		}
		
		assertTrue( a );
	}
	
	public void testListaOrdenadaDesordenada( ) 
	{
		setUp3( );
		ShellSort.shellSort(lista);
		
		boolean a = true;
		int cont = 0;
		while ( cont < lista.length && a )
		{
			a = lista[cont].compareTo( (String) lista[cont + 1] ) < 0? true: false;
			cont++;
		}
		
		assertTrue( a );
	}

	
}
