package test.data_structures;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.QuickSort;
import model.data_structures.ShellSort;

public class TestQuickSort {

	private Comparable<Integer>[] lista;

	public void setUp1( ) 
	{
		lista = new Comparable[10];
		
		lista[0] = 1;
		lista[1] = 2;
		lista[2] = 3;
		lista[3] = 4;
		lista[4] = 5;
		lista[5] = 6;
		lista[6] = 7;
		lista[7] = 8;
		lista[8] = 9;
		lista[9] = 10;

	}

	public void setUp2( ) 
	{
		
		lista[0] = 10;
		lista[1] = 9;
		lista[2] = 8;
		lista[3] = 7;
		lista[4] = 6;
		lista[5] = 5;
		lista[6] = 4;
		lista[7] = 3;
		lista[8] = 2;
		lista[9] = 1;
	}
	
	public void setUp3( ) 
	{
		
		lista[0] = 1045;
		lista[1] = 43445;
		lista[2] = 87685;
		lista[3] = 7787497;
		lista[4] = 69334529;
		lista[5] = 4674827;
		lista[6] = 4542457;
		lista[7] = 379490;
		lista[8] = 267845;
		lista[9] = 145673;
	}

	@Test
	public void testListaOrdenadaAscendentemente( ) 
	{
		setUp1( );
		QuickSort.sort(lista);
		
		boolean a = true;
		int cont = 0;
		while ( cont < lista.length - 1 && a )
		{
			a = lista[cont].compareTo( (Integer) lista[cont + 1] ) < 0? true: false;
			cont++;
		}
		
		assertTrue( lista != null );
		assertTrue( a );
	}
	
	public void testListaOrdenadaDescendentemente( ) 
	{
		setUp2( );
		QuickSort.sort(lista);
		
		boolean a = true;
		int cont = 0;
		while ( cont < lista.length && a )
		{
			a = lista[cont].compareTo( (Integer) lista[cont + 1] ) < 0? true: false;
			cont++;
		}
		
		assertTrue( a );
	}
	
	public void testListaOrdenadaDesordenada( ) 
	{
		setUp3( );
		QuickSort.sort(lista);
		
		boolean a = true;
		int cont = 0;
		while ( cont < lista.length && a )
		{
			a = lista[cont].compareTo( (Integer) lista[cont + 1] ) < 0? true: false;
			cont++;
		}
		
		assertTrue( a );
	}


	
	
	
}
